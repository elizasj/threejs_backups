precision mediump float;

uniform sampler2D  uTexturePosition;
uniform float uTime;
// params
uniform float uFreq;
uniform float vFreq;
uniform float wFreq;
uniform float aCoef;
uniform float bCoef;
uniform float cCoef;
uniform float dCoef;
uniform float eCoef;
uniform float fCoef;

#define PI 3.14159265359

// chladni 2D closed-form solution - returns between -1 and 1
float chladni(float x, float y, float a, float b, float m, float n){
    //float pattern =  a * sin(PI*n*x) * sin(PI*m*y) + b * sin(PI*m*x) * sin(PI*n*y);
    float pattern =  a * sin(PI*n*x) * sin(PI*m*y) + b * sin(PI*m*x) * sin(PI*n*y);// / 
        // cos(n * PI * x) * cos(m * PI * y) - cos(m * PI * x) * cos(n * PI * y);
    return pattern;
}


float customChladni3D(float x, float y, float z, float u, float v, float w, float A, float B, float C, float D, float E, float F){
    // A, B, C, D, E, and F are coefficients that determine the amplitude of each sinusoidal term
    // Higher values will amplify the corresponding frequency in the final pattern
    float pattern = A * sin(u * PI * x) * sin(v * PI * y) * sin(w * PI * z) +
                    B * sin(u * PI * x) * sin(v * PI * z) * sin(w * PI * y) +
                    C * sin(u * PI * y) * sin(v * PI * x) * sin(w * PI * z) +
                    D * sin(u * PI * y) * sin(v * PI * z) * sin(w * PI * x) +
                    E * sin(u * PI * z) * sin(v * PI * x) * sin(w * PI * y) +
                    F * sin(u * PI * z) * sin(v * PI * y) * sin(w * PI * x);

     // Find the maximum absolute value in the pattern
    float maxAmplitude = max(abs(A), max(abs(B), max(abs(C), max(abs(D), max(abs(E), abs(F))))));

    // Scale the entire pattern to ensure it stays within the -1 to 1 range
    pattern /= maxAmplitude;

    // The function returns the final Chladni pattern, which is a superposition of sinusoidal terms
    return pattern;
}


// pseudo random number gen within a give range
// This function uses a common technique to generate a pseudo-random 
// number by taking the dot product of the fragment coordinates and 
// a random-looking vector, then using sin() and fract() to create a 
// number between 0 and 1. Finally, it scales and shifts this number 
// into the specified range.
float random(float low, float high) {
    // Use the fragment's position for a seed
    float seed = dot(gl_FragCoord.xy, vec2(12.9898,78.233));
    // Generate a pseudo-random number
    float random = fract(sin(seed) * 43758.5453123);
    // Scale and shift the number to the desired range
    return low + random * (high - low);
}

float random2(float low, float high) {
    // Use the fragment's position for a seed
    float seed = dot(gl_FragCoord.xy, vec2(2.9898,7.233));
    // Generate a pseudo-random number
    float random = fract(sin(seed) * 43758.5453123);
    // Scale and shift the number to the desired range
    return low + random * (high - low);
}

void main() {
    // uniform vec2 resolution is added automatically to the shader by the GPU 
    // compute class, so there's no need to define it inside your sim frag
    vec2 uv = gl_FragCoord.xy / resolution.xy; 

    // get current positions from the texture from inside the fragment shader 
    // (instead of the vertex shader where they are initially set)
    vec4 tmpPos = texture( uTexturePosition, uv ); 

    // store the position of the particles in a variable
    vec3 position = tmpPos.xyz; 

    // chladni frequency params -- final pattern is the sum of these terms
    float u = uFreq; // 2.0 - freq (determins pattern's dimension)
    float v = vFreq; // 3.0 - freq
    float w = wFreq; // 5.0 - freq
    float A = aCoef; // 1.0 - coef (control the amplitude of the corresponding sinusoidal terms)
    float B = bCoef; // 2.0 - coef
    float C = cCoef; // 1.0 - coef
    float D = dCoef; // 1.0 - coef
    float E = eCoef; // 1.0 - coef
    float F = fCoef; // 1.0 - coef
    
    // initialize position within Chladni pattern
    float eq = customChladni3D(position.x, position.y, position.z, u, v, w, A, B, C, D, E, F);

    // vibration strength params
    float minWalk = 0.02;
    
    // increase the threshold for considering a particle within the Chladni pattern
    if (eq > 0.01) {
        float velocity = 0.05;

        // adjust the probability of applying stochastic movement
        float stochasticProbability = .5;

        // introduce a random factor to determine stochastic movement
        if (random(0.0,.25) < stochasticProbability) {
            float stochasticAmplitude = velocity * abs(eq);
            stochasticAmplitude = clamp(stochasticAmplitude, 0.0, 1.0);

             // Normalize the stochastic amplitude to stay within the 0 to 1 range
            float maxAmplitude = 0.51; // You can adjust this value based on your preferences
            stochasticAmplitude = clamp(stochasticAmplitude / maxAmplitude, 0.0, 1.0);

            //stochasticAmplitude = max(minWalk, stochasticAmplitude);
            position.x += random(-stochasticAmplitude, stochasticAmplitude);
            position.y += random2(-stochasticAmplitude, stochasticAmplitude);

        }
    }

   gl_FragColor = vec4(position, 1. );
}