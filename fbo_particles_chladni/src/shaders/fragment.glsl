precision mediump float;
uniform float uTime; 
uniform float uTscale;
uniform float uPalette;

// colors
uniform vec3 u_a;
uniform vec3 u_b;
uniform vec3 u_c;
uniform vec3 u_d;


in vec2 vUv;
in vec3 pos;
in float noise;

// Cosine based palette, 4 vec3 params
vec3 palette( in float t)
{   
    vec3 a = u_a;
    vec3 b = u_b;
    vec3 c = u_c;
    vec3 d = u_d;

    return a + b*cos( 6.28318*(c*t+d) );
}

void main() {
    //gl_FragColor = vec4(vUv,0.0,1.0);
    //gl_FragColor = vec4(1.);
    //vec3 col = vec3(1., .2, 8.);

    float T = uTime * 0.051;
    vec3 finalColor = vec3(0.0);

    // Apply the same offset to the position to match the repositioning in the vertex shader
    vec3 adjustedPos = pos - vec3(0.5);

    vec3 col = palette(length(adjustedPos) + (uPalette) + sin(T*.5)); // Use distance from the adjusted center to create a cube // add slider here

    float d = max(abs(adjustedPos.x), max(abs(adjustedPos.y), abs(adjustedPos.z))); // Maximum distance to form a cube centered around the adjusted origin
    
    d = sin(d * 2.0) / 4.0; // Create repeating rings around the center point
    //d = abs(d); // Convert negative values to positive


    float osc = mix(0.02, 0.1, cos(T) * 0.15);
    d = pow(osc / d, .85); // Enhance contrast, pow accentuates darker colors closer to 0

    finalColor += col * d; // Combine color with pixel position

    gl_FragColor = vec4(finalColor, 1.0);

    vec3 center = vec3(0.5, 0.5, 0.5);
    float halfSize = .45;
    if (abs(pos.x - center.x) > halfSize || abs(pos.y - center.y) > halfSize || abs(pos.z - center.z) > halfSize) {
        gl_FragColor.a = 0.0;
       return;
       //discard;
    }
    
}