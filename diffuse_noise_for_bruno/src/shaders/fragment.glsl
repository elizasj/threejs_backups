precision mediump float;

//#extension GL_OES_standard_derivatives : enable
	
precision highp float;

uniform vec2 u_resolution;
uniform float u_time;
uniform vec2 u_mouse;

// params
uniform float u_param_controlTime;
uniform float u_param_mouseWidth;
uniform float u_param_density;
uniform float u_param_noiseAmplitude;
uniform vec3 u_param_insideColor;
uniform vec3 u_param_outsideColor;

#define PI 3.14159265
	
// cosPallete
// a - brigthness, b - contrast, c - osc, d - phase
vec3 palette(in vec3 t, in vec3 a, in vec3 b, in vec3 c, in vec3 d) { return a + b*cos( 6.28318*(c*t+d) ); }

float patternZebra(float v, float density){
  float d = 1.0 / density;
  float s = -cos(v / d * PI * 2.);
  return smoothstep(.0, .1 * d, .1 * s / fwidth(s));
}

//
// Description : Array and textureless GLSL 2D simplex noise function.
//      Author : Ian McEwan, Ashima Arts.
//  Maintainer : stegu
//     Lastmod : 20110822 (ijm)
//     License : Copyright (C) 2011 Ashima Arts. All rights reserved.
//               Distributed under the MIT License. See LICENSE file.
//               https://github.com/ashima/webgl-noise
//               https://github.com/stegu/webgl-noise
//

vec3 mod289(vec3 x) {
    return x - floor(x * (1. / 289.)) * 289.;
}

vec2 mod289(vec2 x) {
    return x - floor(x * (1. / 289.)) * 289.;
}

vec3 permute(vec3 x) {
    return mod289(((x * 34.) + 1.) * x);
}

float snoise(vec2 v) {
  const vec4 C = vec4(.211324865405187,.366025403784439,-.577350269189626,.024390243902439);
  vec2 i  = floor(v + dot(v, C.yy) );
  vec2 x0 = v -   i + dot(i, C.xx);
  vec2 i1 = (x0.x > x0.y) ? vec2(1., 0.) : vec2(0., 1.);
  vec4 x12 = x0.xyxy + C.xxzz;
  x12.xy -= i1;
  i = mod289(i);
  vec3 p = permute( permute( i.y + vec3(0., i1.y, 1. )) + i.x + vec3(0., i1.x, 1. ));
  vec3 m = max(0.5 - vec3(dot(x0,x0), dot(x12.xy,x12.xy), dot(x12.zw,x12.zw)), 0.);
  m = m*m;
  m = m*m;
  vec3 x = 2. * fract(p * C.www) - 1.;
  vec3 h = abs(x) - 0.5;
  vec3 ox = floor(x + 0.5);
  vec3 a0 = x - ox;
  m *= 1.79284291400159 - .85373472095314 * ( a0*a0 + h*h );
  vec3 g;
  g.x  = a0.x  * x0.x  + h.x  * x0.y;
  g.yz = a0.yz * x12.xz + h.yz * x12.yw;
  return 130. * dot(m, g);
}

void main() {
	vec2 uv = (gl_FragCoord.xy - u_resolution * .5) / u_resolution.yy; 
	//vec2 uv = (gl_FragCoord.xy / u_resolution);
	float T = u_time * u_param_controlTime; //0.05; // speed up time
	
	float mouseDistance = length(uv - (u_mouse.xy - 0.5)) * .5;
	
    float elevation = uv.y * 0.5;
    float shadow = smoothstep(0.0, u_param_mouseWidth, mouseDistance); // increase width of distortion
    elevation += shadow * u_param_density; //0.5; // increase noise
	
	// Generate noise data
    float amplitude = u_param_noiseAmplitude; //1.5; // noise amplitude
    float frequency = elevation;
    float noiseValue = snoise(uv * frequency) * amplitude + T;// + (iTime *0.1);

	
	// Convert noise data to rings
    //float t = patternZebra(noiseValue, .75);
    // Use smoothstep for smooth blending
    float t = smoothstep(0.4, .9, noiseValue);

	vec3 color = mix(u_param_insideColor, u_param_outsideColor, t); // color swatches
	
	//vec3 col = vec3(.1, .2,.4);
	
    gl_FragColor = vec4(color, 1.0);
	//gl_FragColor = vec4(vec3(mouseDistance), 1.0); // debug mouse behaviour
}