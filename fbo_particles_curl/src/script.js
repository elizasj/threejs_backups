import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'
import { GPUComputationRenderer } from 'three/examples/jsm/misc/GPUComputationRenderer.js';
import * as dat from 'lil-gui'

import vertexShader from './shaders/vertex.glsl'
import fragmentShader from './shaders/fragment.glsl'

// FBO
import simulationFragment from './shaders/simulationFragment.glsl'
/* TEXTURE WIDTH FOR SIMULATION */
const WIDTH = 1000; // states the num of particles we'll be using, you just have to square it

THREE.ColorManagement.enabled = false

/**
 * Base
 */
// Debug
const gui = new dat.GUI()

// Canvas
const canvas = document.querySelector('canvas.webgl')

// Scene
const scene = new THREE.Scene()

let geometry = null
let material = null
let points = null

/**
 * Sizes
 */
const sizes = {
    width: window.innerWidth,
    height: window.innerHeight
}

/**
 * Camera
 */
// Base camera
const camera = new THREE.PerspectiveCamera(35, sizes.width / sizes.height, 0.1, 30)
camera.position.x = 3
camera.position.y = 3
camera.position.z = 3
scene.add(camera)

// Controls
const controls = new OrbitControls(camera, canvas)
controls.enableDamping = true

/**
 * Renderer
 */
const renderer = new THREE.WebGLRenderer({
    canvas: canvas
})
renderer.outputColorSpace = THREE.LinearSRGBColorSpace
renderer.setSize(sizes.width, sizes.height)
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))

/**
 * INITIAL STATE
 *
Texture Positions (fillPositions Function):
These positions are stored in a data texture (dtPosition). This texture acts as a starting point 
for your GPGPU calculations. You're filling this texture with random positions which will then 
be used by the simulationFragment shader. This shader runs on the GPU and updates the positions of 
each particle per frame. The updated texture will contain the new positions 
of the particles after each frame's calculations.
*/
const fillPositions = (texture) => {
    //console.log(texture)
    let arr = texture.image.data

    // each entity in the texture has 4 numbers, so if we set the texture width to be 32, 
    // the size of our position count would be 1024 * 4... 4096 (in td, this is why when you
    // set the pixel format on a noise texture or something, you can select r, rg, rgb, rgba.. 
    // which increases or isolates which data on the texture you want.)
    
    for (let i = 0; i < arr.length; i=i+4) {
        
        let x = Math.random() - 0.5
        let y = Math.random() - 0.5
        let z = Math.random() - 0.5 
        
        arr[i + 0] = x
        arr[i + 1] = y
        arr[i + 2] = z
        arr[i + 3] = 1

    }
   //console.log(arr)
}

/**
 * GPGPU
 */
const initGPGPU = new GPUComputationRenderer(WIDTH, WIDTH, renderer)
const gpuCompute = initGPGPU

if (renderer.capabilities.isWebGL2 === false) {gpuCompute.setDataType( THREE.HalfFloatType );}

// creating a texture that will be updated with the positions of particles and attaching it to a framebuffer.
const dtPosition = gpuCompute.createTexture(); // actually a material/mesh... later we use the uniforms to position thigns
fillPositions(dtPosition)
console.log('step1 - create positions texture data [dynamic - will be updated in shader] - sets the initial state of each particle position in the simulation, which is later updated in the GPU', dtPosition.image.data);

// setting its wrap modes to THREE.RepeatWrapping. This will allow the texture's data to be accessed in a repeating pattern,
// which is sometimes useful for certain effects or simulations.
dtPosition.wrapS = THREE.RepeatWrapping 
dtPosition.wrapT = THREE.RepeatWrapping

// defines how the positions update every frame. When you define a variable with addVariable, the GPUComputationRenderer 
// handles the creation of the necessary framebuffers and textures needed to perform computations on the GPU. it sets up 
// the framework you use to run the computation.
// uFboTexturePosition assigns the name of the texture uniform to use inside simulationFragment.glsl
const posVar = gpuCompute.addVariable('uFboTexturePosition', simulationFragment, dtPosition) 

// adding uniforms to the sim shader
posVar.material.uniforms['uTime'] = { value: 0} // time var for when we later will add curl noise
console.log("step 2 - create framebuffers and textures", posVar)

const error = gpuCompute.init();
if ( error !== null ) {console.error( error );}
gpuCompute.init();


// creating the pieces needed in threejs to see some particles on the screen
const generateParticles = () => {
    if(points !== null)
    {
        geometry.dispose()
        material.dispose()
        scene.remove(points)
    }

    /**
     * Material
     */
    material = new THREE.ShaderMaterial({
        extensions: { derivatives: "#extension GL_OES_standard_derivatives : enable" },
        side: THREE.DoubleSide,
        depthWrite: false,
        blending: THREE.AdditiveBlending,
        vertexColors: true, 
        vertexShader: vertexShader,
        fragmentShader: fragmentShader, 
        uniforms: { // all uniforms are available in both vert & frag
            uTime: { value: 0 }, // updated in tick() function
            uTexturePosition: { value: null }, // updated in tick() function
        }
    })

    /**
     * Geometry
     */
    geometry = new THREE.BufferGeometry()
    const positions = new Float32Array(WIDTH*WIDTH*3) // create a specific number of particles, texture size * 3 for xyz positions
    const reference = new Float32Array(WIDTH*WIDTH*2) // we need to reference every particle in the shader to drive it's position from the position texture.
    
    /*
    Buffer Geometry Positions (generateParticles Function):
    These positions are used to create a buffer for the geometry of the particles that will be 
    rendered on the screen. This buffer does not store the dynamic positions of the particles but 
    rather a static set of positions and reference points (that look like UVs but are actually 
    index references) which are used in the vertex shader to look up the current position of a 
    particle from the data texture that's being updated every frame by the GPGPU calculations.
    */ 
    for(let i = 0; i < WIDTH*WIDTH; i++) {
        // randomly position particles, -.5 to move recenter particle cluster (or else orbit controls roate from top corner)
        let x = Math.random() - .5
        let y = Math.random() - .5
        let z = Math.random() - .5

        let xx = (i%WIDTH)/WIDTH// col num (must be between 0 - 1)
        let yy = ~~(i/WIDTH)/WIDTH// row num (must be between 0 - 1)

        /*
        FYI -- In JavaScript, the ~~ operator is a double bitwise NOT operator. It's used twice in sequence 
        and has the effect of truncating a floating-point number to its integer part, essentially 
        acting like Math.floor() for positive numbers and Math.ceil() for negative numbers. It's a 
        faster substitute because it operates at the bit level, but it only works reliably for numbers 
        within the 32-bit integer range.
        */

        positions.set([x,y,z], i*3) // fill positions 
        reference.set([xx,yy], i*2) // fill virtual uv's for unique id's/refs to each particle
    }
    console.log("step 3 - create buffer geometry positions - placeholder positions [static] - this is a mesh of points rendered on the screen, each point in the mesh looks up its current position from the simulation's state in the GPGPU texture", positions)
    console.log("step 4 - create reference on texture via uv's - this is what allows each point in the mesh to looks up its current position. Very important for the FBO technique to work.", reference)

    // send position info into geometry aka vertex shader, it's a place holder to start 
    // the simulation... the real positions are updated in the dtPosition texture.
    geometry.setAttribute('position', new THREE.BufferAttribute(positions, 3))
    geometry.setAttribute('reference', new THREE.BufferAttribute(reference, 2)) // bridge between placeholder pos, and texture being updated aka the "virtual uvs"
    
    /**
     * Points
     * creating the particle system using THREE.Points. 
     * The particles are using a shader material that 
     * references both the vertex and fragment shaders.
     */
    points = new THREE.Points(geometry, material)
    scene.add(points)

}

/**
 * Animate
 */
const clock = new THREE.Clock()

const tick = () =>
{
    const elapsedTime = clock.getElapsedTime()

    //Update material
    material.uniforms.uTime.value = elapsedTime

    // initiates the rendering to the FBO / running the feedback loop. This process executes the shader code 
    // from simulationFragment for each pixel of the texture, updating it with new positions. 
    gpuCompute.compute()
    // update positions in in the vertex shader with the updated positions from the simFrag shader 
    material.uniforms.uTexturePosition.value = gpuCompute.getCurrentRenderTarget(posVar).texture

    // debug updated positions: create a buffer to read the texture
    let buffer = new Float32Array(WIDTH * WIDTH * 4);
    // renderer.readRenderTargetPixels(
    // gpuCompute.getCurrentRenderTarget(posVar), 
    // 0, 
    // 0, 
    // WIDTH, 
    // WIDTH, 
    // buffer
    // );
    // console.log('step 5. - Updated Positions - The `simulationFragment` shader, running on the GPU, calculates the new positions, and the vertex shader places the particles at these new positions on the screen using the texture as a reference.', buffer);

    // Update controls
    controls.update()

    // Render
    renderer.render(scene, camera)

    // Call tick again on the next frame
    window.requestAnimationFrame(tick)
}

generateParticles()
tick()

window.addEventListener('resize', () =>
{
    // Update sizes
    sizes.width = window.innerWidth
    sizes.height = window.innerHeight

    // Update camera
    camera.aspect = sizes.width / sizes.height
    camera.updateProjectionMatrix()

    // Update renderer
    renderer.setSize(sizes.width, sizes.height)
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
})

