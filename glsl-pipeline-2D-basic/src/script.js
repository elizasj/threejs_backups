import * as THREE from 'three'
//import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'
import * as dat from 'lil-gui'

import { resolveLygia } from 'resolve-lygia';
import { GlslPipeline } from 'glsl-pipeline';

/**
 * Base
 */
// Debug
const gui = new dat.GUI()

let W
let renderer
let glsl_sandbox
let width, height, pixelRatio

function init() {
	const container = document.getElementById("shader");

	W = window
	width = W.innerWidth;
	height = W.innerHeight;
	pixelRatio = W.devicePixelRatio;

	renderer = new THREE.WebGLRenderer();
	renderer.setPixelRatio(window.devicePixelRatio);
	renderer.setSize(window.innerWidth, window.innerHeight);

	container.appendChild(renderer.domElement);

	const shader_frag = resolveLygia(/* glsl */`
      uniform sampler2D   u_doubleBuffer0;
uniform sampler2D	u_buffer0;

uniform vec2        u_resolution;
uniform float       u_time;

#include "lygia/math/const.glsl"
#include "lygia/math/map.glsl"
#include "lygia/space/ratio.glsl"
#include "lygia/space/scale.glsl"
#include "lygia/draw/circle.glsl"
#include "lygia/color/palette.glsl"
#include "lygia/generative/curl.glsl"
#include "lygia/space/kaleidoscope.glsl"

void main() {
  vec3 color = vec3(0.0);
  vec2 pixel = 1.0/u_resolution.xy;
  vec2 st = gl_FragCoord.xy * pixel;

  #if defined(DOUBLE_BUFFER_0) // create a feedback loop
  	// noise
  	float freq = 20.0;
  	vec3 cpos = vec3(st * freq, u_time * 0.25);
	vec3 curly = curl(cpos);
  
    // st.y += 0.0015;
  	float scl = map(cos(u_time), -1.0, 1.0, 0.975, 0.99);
  	st = scale(st, scl);
	st += curly.xy * 0.001;
	// st -= 0.5;
	// st *= 0.99;
	// st += 0.5;
    color = texture2D(u_doubleBuffer0, st).rgb * 0.998;
	
	vec2 cst = ratio(st, u_resolution);
	float circ = circle(cst, 0.25) * 0.015;
	// color += step(distance(cst, vec2(0.5)), 0.25) * 0.015;

	// Gradient color with cosine palette
	float behave = sin(length(cst - 0.5) * 10.0 + u_time);
	vec3 gradient = palette(
		behave,
		vec3(0.5, 0.5, 0.5), // brightness
		vec3(0.5, 0.5, 0.5), // contrast
		vec3(1.0, 1.0, 1.0), // oscillation
		vec3(0.0, 0.05, 0.1) // phase
	);

	color += gradient * circ;
    color = mix(color, vec3(0.0), 0.01);
	
  #elif defined(BUFFER_0)
  	color = texture2D(u_doubleBuffer0, st).rgb; // store feedback in a texture
  #else
  	// st.x += cos(st.x * 50.0) * 0.05;
  	st = kaleidoscope(ratio(st, u_resolution), 8.0, u_time);
  	vec3 img = texture2D(u_buffer0, st).rgb; // sample buffer in a texture
    color = img;

  #endif

   gl_FragColor = vec4(color, 1.0);
}`);

	// GLSL Buffers
	glsl_sandbox = new GlslPipeline(renderer);
	glsl_sandbox.load(shader_frag);

	onWindowResize();
	window.addEventListener("resize", onWindowResize);
}

function onWindowResize() {
	width = W.innerWidth;
	height = W.innerHeight;
	pixelRatio = W.devicePixelRatio;

	renderer.setPixelRatio(pixelRatio);
	renderer.setSize(width, height);

	glsl_sandbox.setSize(width, height);
}

let runOnce = false
function render() {
	glsl_sandbox.renderMain();
}

function animate() {
	render();
	requestAnimationFrame(animate);
}

init();
animate();

// document.onmousemove = function (e) {
// 	uniforms.u_mouse.value.x = e.pageX / window.innerWidth;
//     uniforms.u_mouse.value.y = 1.0 - e.pageY / window.innerHeight;
	
// };
