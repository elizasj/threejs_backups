precision mediump float;

in vec2 vUv;

void main() {
    gl_FragColor = vec4(vUv,0.0,1.0);
    //gl_FragColor = vec4(1.0);
}