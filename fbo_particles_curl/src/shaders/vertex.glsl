precision mediump float;

uniform float uTime; 

out vec2 vUv;

uniform sampler2D uTexturePosition; // gets updated in the tick fn.

// position attribute is already here by default so no need to declare it
attribute vec2 reference;

float PI = 3.141592653589793238;

void main() {
    //vUv = uv;
    vUv = reference;

    // read updating positions from texture, store in var
    vec3 pos = texture(uTexturePosition, reference).xyz;

    // vec4 mvPosition = modelViewMatrix * vec4(position, 1.); // initial positions colored by ref. Works!
    vec4 mvPosition = modelViewMatrix * vec4(pos, 1.); // updated positions are all the same :( 

    gl_PointSize =  10. * (1. / -mvPosition.z);
    gl_Position = projectionMatrix * mvPosition;
}
