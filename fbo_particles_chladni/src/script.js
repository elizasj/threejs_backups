import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'
import { GPUComputationRenderer } from 'three/examples/jsm/misc/GPUComputationRenderer.js';
import * as dat from 'lil-gui'
import Stats from 'stats.js'

import vertexShader from './shaders/vertex.glsl'
import fragmentShader from './shaders/fragment.glsl'

// FBO
import simulationFragment from './shaders/simulationFragment.glsl'
/* TEXTURE WIDTH FOR SIMULATION */
const WIDTH = 1000; // states the num of particles we'll be using, you just have to square it

THREE.ColorManagement.enabled = false

// FPS
const stats = new Stats()
stats.showPanel(0) // 0: fps, 1: ms, 2: mb, 3+: custom
document.body.appendChild(stats.dom)

/**
 * Base
 */

// colors
const colorSets = [
    {
        // "OG"
        a: [0.798, 0.380, 0.428],
        b: [0.730, 0.345, -0.194],
        c: [0.405, 1.642, 1.235],
        d: [-0.273, 0.657, 0.097]
    },
    {
        // "FAVE"
        a: [0.577, 0.469, 0.887],
        b: [0.468, 0.245, 0.846],
        c: [1.323, 0.752, 0.758],
        d: [4.312, 2.436, 3.669]
    },
    {
        // "ALSO NICE - MANY COLORS, GOOD BRIGHTNESS"
        a: [0.779, 0.572, 0.278],
        b: [0.808, 0.007, 0.388],
        c: [1.037, 0.601, 0.395],
        d: [2.714, 4.033, 1.727]
    },
    {
        // "VERY BLUE AND GREEN BUT NICE"
        a: [0.216, 0.652, 0.486],
        b: [0.255, 0.228, 0.546],
        c: [0.145, 0.837, 0.988],
        d: [4.876, 4.893, 6.231]
    },
    {
        // "LOVE - LOTS OF COLORS"
        a: [0.929, 0.523, 0.300],
        b: [0.634, 0.899, 0.471],
        c: [1.512, 1.133, 0.776],
        d: [5.309, 0.479, 2.377]
    },
    {
        //"FALL COLOR VIBES"
        a: [0.575, 0.269, 0.204],
        b: [0.602, 0.041, 0.183],
        c: [1.497, 0.521, 1.476],
        d: [2.305, 1.001, 0.467]
    },
    {
        // "HANDLES BLUES WELL"
        a: [0.882, 0.591, 0.889],
        b: [0.652, 0.250, 0.269],
        c: [1.505, 0.563, 0.553],
        d: [1.071, 1.773, 2.219]
    }
];

// Function to select a random group
function selectRandomGroup() {
    const index = Math.floor(Math.random() * colorSets.length);
    return colorSets[index];
}
let selectedCol = selectRandomGroup();

const palette = {
    u_a: { value: new THREE.Vector3() },
    u_b: { value: new THREE.Vector3() },
    u_c: { value: new THREE.Vector3() },
    u_d: { value: new THREE.Vector3() }
};

//frequencies
function generateUniqueNumbers(rangeStart, rangeEnd, count) {
    if (count > (rangeEnd - rangeStart + 1)) {
        throw new Error("Cannot generate more unique numbers than the size of the range");
    }

    let numbers = [];
    while (numbers.length < count) {
        let randomNumber = Math.floor(Math.random() * (rangeEnd - rangeStart + 1)) + rangeStart;
        if (!numbers.includes(randomNumber)) {
            numbers.push(randomNumber);
        }
    }

    return numbers;
}

// Generate 3 unique numbers between 1 and 5
let frequencies = generateUniqueNumbers(1, 5, 3);

// Generate 6 unique numbers between 1 and 10
let coeficients = generateUniqueNumbers(1, 10, 6);

console.log(frequencies, coeficients);

// Debug
const gui = new dat.GUI()

const parameters = {}
parameters.uTime = 0.0
parameters.uTscale = 0.088
parameters.uNoiseFreq = 1.76
parameters.uNoiseAmp =  0.03
parameters.uPalette = 0.3 + Math.random() * 0.7; // generate a rand num betweem 0.3 and 1.
parameters.uFreq = frequencies[0] //2.0
parameters.vFreq = frequencies[1] //3.0
parameters.wFreq = frequencies[2] //5.0
parameters.aCoef = coeficients[0] //1.0
parameters.bCoef = coeficients[1] //2.0
parameters.cCoef = coeficients[2] //1.0
parameters.dCoef = coeficients[3] //1.0
parameters.eCoef = coeficients[4] //1.0
parameters.fCoef = coeficients[5] //1.0

// Canvas
const canvas = document.querySelector('canvas.webgl')

// Scene
const scene = new THREE.Scene()

let geometry = null
let material = null
let points = null

/**
 * Sizes
 */
const sizes = {
    width: window.innerWidth,
    height: window.innerHeight
}

/**
 * Camera
 */
// Base camera
const camera = new THREE.PerspectiveCamera(20, sizes.width / sizes.height, 0.1, 50)
camera.position.x = 3
camera.position.y = 3
camera.position.z = 3
scene.add(camera)

// Controls
const controls = new OrbitControls(camera, canvas)
controls.enableDamping = true

/**
 * Renderer
 */
const renderer = new THREE.WebGLRenderer({
    canvas: canvas
})
renderer.outputColorSpace = THREE.LinearSRGBColorSpace
renderer.setSize(sizes.width, sizes.height)
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))

/**
 * INITIAL STATE
 *
Texture Positions (fillPositions Function):
These positions are stored in a data texture (dtPosition). This texture acts as a starting point 
for your GPGPU calculations. You're filling this texture with random positions which will then 
be used by the simulationFragment shader. This shader runs on the GPU and updates the positions of 
each particle per frame. The updated texture will contain the new positions 
of the particles after each frame's calculations.
*/
const fillPositions = (texture) => {
    //console.log(texture)
    let arr = texture.image.data

    // each entity in the texture has 4 numbers, so if we set the texture width to be 32, 
    // the size of our position count would be 1024 * 4... 4096 (in td, this is why when you
    // set the pixel format on a noise texture or something, you can select r, rg, rgb, rgba.. 
    // which increases or isolates which data on the texture you want.)
    
    for (let i = 0; i < arr.length; i=i+4) {

        let x = Math.random()
        let y = Math.random()
        let z = Math.random()

        arr[i + 0] = x
        arr[i + 1] = y
        arr[i + 2] = z
        arr[i + 3] = 1

    }
   //console.log(arr)
}

/**
 * GPGPU
 */
const initGPGPU = new GPUComputationRenderer(WIDTH, WIDTH, renderer)
const gpuCompute = initGPGPU

if (renderer.capabilities.isWebGL2 === false) {gpuCompute.setDataType( THREE.HalfFloatType );}

// creating a texture that will be updated with the positions of particles and attaching it to a framebuffer.
const dtPosition = gpuCompute.createTexture(); // actually a material/mesh... later we use the uniforms to position thigns
// defines how the positions update every frame. When you define a variable with addVariable, the GPUComputationRenderer 
// handles the creation of the necessary framebuffers and textures needed to perform computations on the GPU. it sets up 
// the framework you use to run the computation.
// uFboTexturePosition assigns the name of the texture uniform to use inside simulationFragment.glsl
const posVar = gpuCompute.addVariable('uFboTexturePosition', simulationFragment, dtPosition) 

const runGPGPUthings = () => {
    fillPositions(dtPosition)
    //console.log('step1 - create positions texture data [dynamic - will be updated in shader] - sets the initial state of each particle position in the simulation, which is later updated in the GPU', dtPosition.image.data);

    // setting its wrap modes to THREE.RepeatWrapping. This will allow the texture's data to be accessed in a repeating pattern,
    // which is sometimes useful for certain effects or simulations.
    dtPosition.wrapS = THREE.RepeatWrapping 
    dtPosition.wrapT = THREE.RepeatWrapping


    // adding uniforms to the sim shader
    posVar.material.uniforms['uTime'] = { value: parameters.uTime } // time var for when we later will add noise
    posVar.material.uniforms['uFreq'] = { value: parameters.uFreq }
    posVar.material.uniforms['vFreq'] = { value: parameters.vFreq }
    posVar.material.uniforms['wFreq'] = { value: parameters.wFreq }
    posVar.material.uniforms['aCoef'] = { value: parameters.aCoef }
    posVar.material.uniforms['bCoef'] = { value: parameters.bCoef }
    posVar.material.uniforms['cCoef'] = { value: parameters.cCoef }
    posVar.material.uniforms['dCoef'] = { value: parameters.dCoef }
    posVar.material.uniforms['eCoef'] = { value: parameters.eCoef }
    posVar.material.uniforms['fCoef'] = { value: parameters.fCoef }

    const error = gpuCompute.init();
    if ( error !== null ) {console.error( error );}
    gpuCompute.init();
}


// creating the pieces needed in threejs to see some particles on the screen
const generateParticles = () => {
    console.log('generateParticles() function is executing!');
    // Dispose of previous resources
    if (points !== null) {
        scene.remove(points);
        points.geometry.dispose();
        points.material.dispose();
    }

    /**
     * Material
     */
    material = new THREE.ShaderMaterial({
        extensions: { derivatives: "#extension GL_OES_standard_derivatives : enable" },
        side: THREE.DoubleSide,
        depthWrite: false,
        transparent: true,
        blending: THREE.AdditiveBlending,
        vertexColors: true, 
        vertexShader: vertexShader,
        fragmentShader: fragmentShader, 
        uniforms: { // all uniforms are available in both vert & frag
            uTime: { value: 0 }, // updated in tick() function
            uTexturePosition: { value: null }, // updated in tick() function
            uTscale: { value: parameters.uTscale }, 
            uNoiseFreq: { value: parameters.uNoiseFreq },
            uNoiseAmp: { value: parameters.uNoiseAmp },
            uPalette: { value: parameters.uPalette }, // playing within the palette's range
            u_a: { value: palette.u_a.value.set(...selectedCol.a) },
            u_b: { value: palette.u_b.value.set(...selectedCol.b) },
            u_c: { value: palette.u_c.value.set(...selectedCol.c) },
            u_d: { value: palette.u_d.value.set(...selectedCol.d) }
        }
    })

    /**
     * Geometry
     */
    geometry = new THREE.BufferGeometry()
    const positions = new Float32Array(WIDTH*WIDTH*3) // create a specific number of particles, texture size * 3 for xyz positions
    const reference = new Float32Array(WIDTH*WIDTH*2) // we need to reference every particle in the shader to drive it's position from the position texture.
    
    /*
    Buffer Geometry Positions (generateParticles Function):
    These positions are used to create a buffer for the geometry of the particles that will be 
    rendered on the screen. This buffer does not store the dynamic positions of the particles but 
    rather a static set of positions and reference points (that look like UVs but are actually 
    index references) which are used in the vertex shader to look up the current position of a 
    particle from the data texture that's being updated every frame by the GPGPU calculations.
    */ 
    for(let i = 0; i < WIDTH*WIDTH; i++) {
        // randomly position particles, -.5 to move recenter particle cluster (or else orbit controls roate from top corner)
        let x = Math.random()
        let y = Math.random()
        let z = Math.random()

        let xx = (i%WIDTH)/WIDTH// col num (must be between 0 - 1)
        let yy = ~~(i/WIDTH)/WIDTH// row num (must be between 0 - 1)

        /*
        FYI -- In JavaScript, the ~~ operator is a double bitwise NOT operator. It's used twice in sequence 
        and has the effect of truncating a floating-point number to its integer part, essentially 
        acting like Math.floor() for positive numbers and Math.ceil() for negative numbers. It's a 
        faster substitute because it operates at the bit level, but it only works reliably for numbers 
        within the 32-bit integer range.
        */

        positions.set([x,y,z], i*3) // fill positions 
        reference.set([xx,yy], i*2) // fill virtual uv's for unique id's/refs to each particle
    }
    //console.log("step 3 - create buffer geometry positions - placeholder positions [static] - this is a mesh of points rendered on the screen, each point in the mesh looks up its current position from the simulation's state in the GPGPU texture", positions)
    //console.log("step 4 - create reference on texture via uv's - this is what allows each point in the mesh to looks up its current position. Very important for the FBO technique to work.", reference)

    // send position info into geometry aka vertex shader, it's a place holder to start 
    // the simulation... the real positions are updated in the dtPosition texture.
    geometry.setAttribute('position', new THREE.BufferAttribute(positions, 3))
    geometry.setAttribute('reference', new THREE.BufferAttribute(reference, 2)) // bridge between placeholder pos, and texture being updated aka the "virtual uvs"
    
    /**
     * Points
     * creating the particle system using THREE.Points. 
     * The particles are using a shader material that 
     * references both the vertex and fragment shaders.
     */
    points = new THREE.Points(geometry, material)
    scene.add(points)
}


// Vert params
gui.add(parameters, 'uTscale').min(0.01).max(0.25).step(0.001).onFinishChange(generateParticles)
gui.add(parameters, 'uNoiseFreq').min(0.01).max(2.0).step(0.01).onFinishChange(generateParticles)
gui.add(parameters, 'uNoiseAmp').min(0.01).max(0.5).step(0.001).onFinishChange(generateParticles)
gui.add(parameters, 'uPalette').min(0.).max(1.).step(0.1).onFinishChange(generateParticles)

// FBO params
gui.add(parameters, 'uFreq').min(1.).max(5.0).step(1.).onFinishChange(() => {
     // Update GPUComputationRenderer uniform
     posVar.material.uniforms['uFreq'].value = parameters.uFreq;
     console.log('Manual uFreq Update:', parameters.uFreq);
 
     // Reset positions
     runGPGPUthings()
     // Generate new particles
     generateParticles();
});

gui.add(parameters, 'vFreq').min(1.).max(5.0).step(1.).onFinishChange(() => {
    // Update GPUComputationRenderer uniform
    posVar.material.uniforms['uFreq'].value = parameters.uFreq;
    console.log('Manual uFreq Update:', parameters.uFreq);

    // Reset positions
    runGPGPUthings()
    // Generate new particles
    generateParticles();
});

gui.add(parameters, 'wFreq').min(1.).max(5.0).step(1.).onFinishChange(() => {
    // Update GPUComputationRenderer uniform
    posVar.material.uniforms['uFreq'].value = parameters.uFreq;
    console.log('Manual uFreq Update:', parameters.uFreq);

    // Reset positions
    runGPGPUthings()
    // Generate new particles
    generateParticles();
});

gui.add(parameters, 'aCoef').min(1.).max(10.0).step(1.).onFinishChange(() => {
    // Update GPUComputationRenderer uniform
    posVar.material.uniforms['uFreq'].value = parameters.uFreq;
    console.log('Manual uFreq Update:', parameters.uFreq);

    // Reset positions
    runGPGPUthings()
    // Generate new particles
    generateParticles();
});

gui.add(parameters, 'bCoef').min(1.).max(10.0).step(1.).onFinishChange(() => {
    // Update GPUComputationRenderer uniform
    posVar.material.uniforms['uFreq'].value = parameters.uFreq;
    console.log('Manual uFreq Update:', parameters.uFreq);

    // Reset positions
    runGPGPUthings()
    // Generate new particles
    generateParticles();
});

gui.add(parameters, 'cCoef').min(1.).max(10.0).step(1.).onFinishChange(() => {
    // Update GPUComputationRenderer uniform
    posVar.material.uniforms['uFreq'].value = parameters.uFreq;
    console.log('Manual uFreq Update:', parameters.uFreq);

    // Reset positions
    runGPGPUthings()
    // Generate new particles
    generateParticles();
});


gui.add(parameters, 'dCoef').min(1.).max(10.0).step(1.).onFinishChange(() => {
    // Update GPUComputationRenderer uniform
    posVar.material.uniforms['uFreq'].value = parameters.uFreq;
    console.log('Manual uFreq Update:', parameters.uFreq);

    // Reset positions
    runGPGPUthings()
    // Generate new particles
    generateParticles();
});

gui.add(parameters, 'eCoef').min(1.).max(10.0).step(1.).onFinishChange(() => {
    // Update GPUComputationRenderer uniform
    posVar.material.uniforms['uFreq'].value = parameters.uFreq;
    console.log('Manual uFreq Update:', parameters.uFreq);

    // Reset positions
    runGPGPUthings()
    // Generate new particles
    generateParticles();
});

gui.add(parameters, 'fCoef').min(1.).max(10.0).step(1.).onFinishChange(() => {
    // Update GPUComputationRenderer uniform
    posVar.material.uniforms['uFreq'].value = parameters.uFreq;
    console.log('Manual uFreq Update:', parameters.uFreq);

    // Reset positions
    runGPGPUthings()
    // Generate new particles
    generateParticles();
});



/**
 * Animate
 */
const clock = new THREE.Clock()

const tick = () =>
{
    stats.begin()

    const elapsedTime = clock.getElapsedTime()

    //Update material
    material.uniforms.uTime.value = elapsedTime
    posVar.material.uniforms['uTime'].value = elapsedTime

    // initiates the rendering to the FBO / running the feedback loop. This process executes the shader code 
    // from simulationFragment for each pixel of the texture, updating it with new positions. 
    gpuCompute.compute()
    // update positions in in the vertex shader with the updated positions from the simFrag shader 
    material.uniforms.uTexturePosition.value = gpuCompute.getCurrentRenderTarget(posVar).texture

    // debug updated positions: create a buffer to read the texture
    let buffer = new Float32Array(WIDTH * WIDTH * 4);
    // renderer.readRenderTargetPixels(
    // gpuCompute.getCurrentRenderTarget(posVar), 
    // 0, 
    // 0, 
    // WIDTH, 
    // WIDTH, 
    // buffer
    // );
    // console.log('step 5. - Updated Positions - The `simulationFragment` shader, running on the GPU, calculates the new positions, and the vertex shader places the particles at these new positions on the screen using the texture as a reference.', buffer);

    // Update controls
    controls.update()

    // Render
    renderer.render(scene, camera)

    // Call tick again on the next frame
    window.requestAnimationFrame(tick)

    stats.end()
}

runGPGPUthings()
generateParticles()
tick()

window.addEventListener('resize', () =>
{
    // Update sizes
    sizes.width = window.innerWidth
    sizes.height = window.innerHeight

    // Update camera
    camera.aspect = sizes.width / sizes.height
    camera.updateProjectionMatrix()

    // Update renderer
    renderer.setSize(sizes.width, sizes.height)
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
})

// test variations
function updateSettings() {
    runGPGPUthings() 
    selectedCol = selectRandomGroup();
    frequencies = generateUniqueNumbers(1, 5, 3);
    coeficients = generateUniqueNumbers(1, 10, 6);

    // Update shader uniforms
    material.uniforms.u_a.value.set(...selectedCol.a);
    material.uniforms.u_b.value.set(...selectedCol.b);
    material.uniforms.u_c.value.set(...selectedCol.c);
    material.uniforms.u_d.value.set(...selectedCol.d);
    
    // If other parameters like frequencies and coefficients are used in shaders, update them too
    posVar.material.uniforms.uFreq.value = frequencies[0] //2.0
    posVar.material.uniforms.vFreq.value = frequencies[1] //3.0
    posVar.material.uniforms.wFreq.value = frequencies[2] //5.0
    posVar.material.uniforms.bCoef.value = coeficients[1] //2.0
    posVar.material.uniforms.cCoef.value = coeficients[2] //1.0
    posVar.material.uniforms.dCoef.value = coeficients[3] //1.0
    posVar.material.uniforms.eCoef.value = coeficients[4] //1.0
    posVar.material.uniforms.fCoef.value = coeficients[5] //1.0

    // Optionally regenerate particles or update the scene if necessary

    generateParticles(); // Uncomment this if you need to regenerate the particle system
}

// On page load, update settings initially
document.addEventListener("DOMContentLoaded", function() {
    updateSettings();
});

// Update settings every 3 seconds
//setInterval(updateSettings, 3000);
