precision mediump float;

uniform sampler2D  uTexturePosition;

void main()	{
    // uniform vec2 resolution is added automatically to the shader by the gpu 
    // compute class, so there's no need to define it inside your sim frag
    vec2 uv = gl_FragCoord.xy / resolution.xy; 

    // get current positions from texture from inside the fragment shader 
    // (instead of the vertex shader where they are initially set)
    vec4 tmpPos = texture( uTexturePosition, uv ); 
    

    // store the position of the partciles in a variable
    vec3 position = tmpPos.xyz; 

    // increment the positions
    // (the cloud of particles should be moving toward the screen)
    gl_FragColor = vec4( position + vec3(0.01), 1. );

}
