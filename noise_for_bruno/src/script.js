import * as THREE from 'three'
//import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'
import * as dat from 'lil-gui'

import vertexShader from './shaders/vertex.glsl'
import fragmentShader from './shaders/fragment.glsl'


/**
 * Base
 */
// Debug
const gui = new dat.GUI()

let camera, scene, renderer, clock;
let uniforms;
let parameters;

function init() {
	const container = document.getElementById("shader");

	clock = new THREE.Clock();
	camera = new THREE.Camera();
	camera.position.z = 1;

	scene = new THREE.Scene();

	const geometry = new THREE.PlaneBufferGeometry(2, 2);

	uniforms = {
		u_time: { type: "f", value: 1.0 },
		u_resolution: { type: "v2", value: new THREE.Vector2() },
		u_mouse: { type: "v2", value: new THREE.Vector2() },
        // GUI elements for testing:
        u_param_controlTime : { type : "f", value: 0.05},
        u_param_mouseWidth : { type : "f", value: 0.25},
        u_param_density : { type : "f", value: 0.5},
        u_param_noiseAmplitude : { type : "f", value: 1.5},
        u_param_insideColor: { value: new THREE.Color('#c7b2ff') },
        u_param_outsideColor: { value: new THREE.Color('#c9d9ff') },
	};

	const material = new THREE.ShaderMaterial({
		uniforms,
		vertexShader: vertexShader,
		fragmentShader: fragmentShader
	});


	const mesh = new THREE.Mesh(geometry, material);
	scene.add(mesh);

	renderer = new THREE.WebGLRenderer();
	renderer.setPixelRatio(window.devicePixelRatio);

	container.appendChild(renderer.domElement);

    gui.add(material.uniforms.u_param_controlTime, 'value').min(0.01).max(0.5).step(.01).name(" < slower | faster >")
    gui.add(material.uniforms.u_param_mouseWidth, 'value').min(0.1).max(1).step(.1).name("mouse radius")
    gui.add(material.uniforms.u_param_density, 'value').min(0.5).max(5.).step(.1).name("pattern density")
    gui.add(material.uniforms.u_param_noiseAmplitude, 'value').min(0.5).max(5.).step(.1).name("noise amplitude")
    gui.addColor(material.uniforms.u_param_insideColor, 'value');
    gui.addColor(material.uniforms.u_param_outsideColor, 'value');


	onWindowResize();
	window.addEventListener("resize", onWindowResize);
}



function onWindowResize() {
	renderer.setSize(window.innerWidth, window.innerHeight);
	uniforms.u_resolution.value.x = renderer.domElement.width;
	uniforms.u_resolution.value.y = renderer.domElement.height;
}

let runOnce = false
function render() {
	uniforms.u_time.value = clock.getElapsedTime();
	renderer.render(scene, camera);
}

function animate() {
	render();
	requestAnimationFrame(animate);
}

init();
animate();

document.onmousemove = function (e) {
	uniforms.u_mouse.value.x = e.pageX / window.innerWidth;
    uniforms.u_mouse.value.y = 1.0 - e.pageY / window.innerHeight;
	
};
